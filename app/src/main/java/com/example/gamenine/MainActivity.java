package com.example.gamenine;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button[][] buttons = new Button[3][3];

    private boolean player1Turn;

    private int roundCount;

    private int player1Points;
    private int player2Points;

    private TextView textViewPlayer1;
    private TextView textViewPlayer1Points;
    private TextView textViewPlayer2;
    private TextView textViewPlayer2Points;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewPlayer1 = findViewById(R.id.txtPlayer1);
        textViewPlayer1Points = findViewById(R.id.txtPlayer1Points);
        textViewPlayer2 = findViewById(R.id.txtPlayer2);
        textViewPlayer2Points = findViewById(R.id.txtPlayer2Points);

        Random random = new Random();
        player1Turn = random.nextInt(10) >= 5;
        if (player1Turn) {
            textViewPlayer1.setTextColor(getResources().getColor(R.color.playerTurnColor));
        } else {
            textViewPlayer2.setTextColor(getResources().getColor(R.color.playerTurnColor));
        }

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                String buttonID = "btn_" + i + j;
                int resID = getResources().getIdentifier(buttonID, "id", getPackageName());
                buttons[i][j] = findViewById(resID);
                buttons[i][j].setOnClickListener(this);
            }
        }

        Button buttonReset = findViewById(R.id.btnRest);
        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetGame();
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (!((Button) v).getText().toString().equals("")) {
            return;
        }

        if (player1Turn) {
            ((Button) v).setText("X");
            isTurn();
        } else {
            ((Button) v).setText("O");
            isTurn();

        }

        roundCount++;

        if (checkForWin()) {
            if (player1Turn) {
                player1Wins();

            } else {
                player2Wins();
                player1Turn = false;

            }
        } else if (roundCount == 9) {
            draw();
        } else {
            player1Turn = !player1Turn;
        }

    }

    private boolean checkForWin() {
        String[][] field = new String[3][3];

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                field[i][j] = buttons[i][j].getText().toString();
            }
        }

        for (int i = 0; i < 3; i++) {
            if (field[i][0].equals(field[i][1])
                    && field[i][0].equals(field[i][2])
                    && !field[i][0].equals("")) {
                return true;
            }
        }

        for (int i = 0; i < 3; i++) {
            if (field[0][i].equals(field[1][i])
                    && field[0][i].equals(field[2][i])
                    && !field[0][i].equals("")) {
                return true;
            }
        }

        if (field[0][0].equals(field[1][1])
                && field[0][0].equals(field[2][2])
                && !field[0][0].equals("")) {
            return true;
        }

        return field[0][2].equals(field[1][1])
                && field[0][2].equals(field[2][0])
                && !field[0][2].equals("");
    }

    private void player1Wins() {
        player1Points++;
        Toast.makeText(this, "Player 1 wins!", Toast.LENGTH_SHORT).show();
        textViewPlayer1Points.setText("" + player1Points);
        resetBoard();
        textViewPlayer2.setTextColor(getResources().getColor(R.color.playerDefaultColor));
        textViewPlayer1.setTextColor(getResources().getColor(R.color.playerTurnColor));

    }

    private void player2Wins() {
        player2Points++;
        Toast.makeText(this, "Player 2 wins!", Toast.LENGTH_SHORT).show();
        textViewPlayer2Points.setText("" + player2Points);
        resetBoard();
        textViewPlayer1.setTextColor(getResources().getColor(R.color.playerDefaultColor));
        textViewPlayer2.setTextColor(getResources().getColor(R.color.playerTurnColor));

    }

    private void draw() {
        Toast.makeText(this, "Draw!", Toast.LENGTH_SHORT).show();
        resetBoard();
    }

    private void resetBoard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                buttons[i][j].setText("");
            }
        }

        roundCount = 0;

    }

    private void resetGame() {
        player1Points = 0;
        player2Points = 0;
        textViewPlayer1Points.setText("");
        textViewPlayer2Points.setText("");
        resetBoard();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("roundCount", roundCount);
        outState.putInt("player1Points", player1Points);
        outState.putInt("player2Points", player2Points);
        outState.putBoolean("player1Turn", player1Turn);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        roundCount = savedInstanceState.getInt("roundCount");
        player1Points = savedInstanceState.getInt("player1Points");
        player2Points = savedInstanceState.getInt("player2Points");
        player1Turn = savedInstanceState.getBoolean("player1Turn");
    }

    private void isTurn() {
        if (player1Turn) {
            textViewPlayer2.setTextColor(getResources().getColor(R.color.playerTurnColor));
            textViewPlayer1.setTextColor(getResources().getColor(R.color.playerDefaultColor));
        } else {
            textViewPlayer1.setTextColor(getResources().getColor(R.color.playerTurnColor));
            textViewPlayer2.setTextColor(getResources().getColor(R.color.playerDefaultColor));
        }
    }

}

